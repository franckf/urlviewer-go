#! /bin/sh

mkdir -p bin
for i in $(ls -d */ | grep -v bin | sed 's/.$//')
    do
        echo $i
        cd $i
        go build main.go
        mv main ../bin/$i
        cd ..
done
