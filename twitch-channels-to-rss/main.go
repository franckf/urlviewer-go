package main

import (
	"bytes"
	_ "embed"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io"
	"net/http"
	"strconv"
	"strings"
	"time"
)

//go:embed .env
var env string
var clientid string = strings.Split(env, " ")[0]
var sha string = strings.Split(env, " ")[1]

const graphql string = "https://gql.twitch.tv/gql"

var streamers = []string{"angledroit", "grafikart", "ultia", "rivenzi", "ponce", "mynthos", "theprimeagen", "jussetain", "joueur_du_grenier", "bagherajones", "hortyunderscore", "avamind", "antoinedaniel"}

type reqgql struct {
	OperationName string          `json:"operationName"`
	Variables     reqgqlVariable  `json:"variables"`
	Extensions    reqgqlExtension `json:"extensions"`
}
type reqgqlVariable struct {
	ChannelOwnerLogin string `json:"channelOwnerLogin"`
	Limit             int    `json:"limit"`
	BroadcastType     string `json:"broadcastType"`
	VideoSort         string `json:"videoSort"`
}
type reqgqlExtension struct {
	PersistedQuery reqgqlPersistedQuery `json:"persistedQuery"`
}
type reqgqlPersistedQuery struct {
	Version    int    `json:"version"`
	Sha256Hash string `json:"sha256Hash"`
}

type resgql struct {
	Data resData `json:"data"`
}
type resData struct {
	User resUser `json:"user"`
}
type resUser struct {
	Videos resVideos `json:"videos"`
}
type resVideos struct {
	Edges []resEdge `json:"edges"`
}
type resEdge struct {
	Node resNode `json:"node"`
}
type resNode struct {
	Title               string `json:"title"`
	Id                  string `json:"id"`
	AnimatedPreviewURL  string `json:"animatedPreviewURL"`
	PreviewThumbnailURL string `json:"previewThumbnailURL"`
}

type video struct {
	Streamer  string
	Title     string
	Id        int
	Thumbnail string
	Gif       string
}

type rss struct {
	Version     string    `xml:"version,attr"`
	Title       string    `xml:"channel>title"`
	Link        string    `xml:"channel>link"`
	Description string    `xml:"channel>description"`
	PubDate     string    `xml:"channel>pubDate"`
	Item        []rssItem `xml:"channel>item"`
}

type rssItem struct {
	Title       string `xml:"title"`
	Link        string `xml:"link"`
	Description string `xml:"description"`
}

func main() {
	ch := make(chan []video)
	allVideos := []video{}
	for _, streamer := range streamers {
		go reqTwitchParChannel(ch, streamer)
	}

	for i := 0; i < len(streamers); i++ {
		//for current := range ch {
		current := <-ch
		allVideos = append(allVideos, current...)
	}

	rssChannels := convertVideosToRss(allVideos)
	pubdateTime := time.Now().Format(time.RubyDate)
	rssFeed := rss{
		Version:     "2.0",
		Title:       "twitch channels",
		Link:        "https://www.twitch.tv/",
		Description: "autogen rss from twitch channels",
		PubDate:     pubdateTime,
		Item:        rssChannels,
	}
	data, _ := xml.MarshalIndent(rssFeed, "", "    ")
	fmt.Println(string(data))
}

func reqTwitchParChannel(ch chan []video, streamer string) {
	current := reqgql{
		OperationName: "FilterableVideoTower_Videos",
		Variables: reqgqlVariable{
			ChannelOwnerLogin: streamer,
			Limit:             30,
			BroadcastType:     "ARCHIVE",
			VideoSort:         "TIME",
		},
		Extensions: reqgqlExtension{
			PersistedQuery: reqgqlPersistedQuery{
				Version:    1,
				Sha256Hash: sha,
			},
		},
	}
	resCurrent := requestTwitch(current)
	channelCurrent := parseTwitchChannel(streamer, resCurrent)
	ch <- channelCurrent
}

func convertVideosToRss(allVideos []video) (rssChannels []rssItem) {
	for _, video := range allVideos {
		var currentItem rssItem
		currentItem.Title = video.Streamer + "-" + video.Title
		currentItem.Link = "https://www.twitch.tv/videos/" + strconv.Itoa(video.Id)
		currentItem.Description = `
        <img src="` + video.Thumbnail + `" title="` + video.Streamer + `" />
        <img src="` + video.Gif + `" title="` + video.Title + `" />
        `
		rssChannels = append(rssChannels, currentItem)
	}
	return
}

func parseTwitchChannel(streamer string, jsonRes []byte) (videos []video) {
	var data []resgql
	err := json.Unmarshal(jsonRes, &data)
	if err != nil {
		fmt.Println("unmarshalling", err)
	}
	for _, v := range data[0].Data.User.Videos.Edges {
		var newVideo video
		newVideo.Streamer = streamer
		newVideo.Title = v.Node.Title
		newVideo.Id, _ = strconv.Atoi(v.Node.Id)
		newVideo.Gif = v.Node.AnimatedPreviewURL
		newVideo.Thumbnail = v.Node.PreviewThumbnailURL
		videos = append(videos, newVideo)
	}
	return
}

func requestTwitch(body reqgql) (response []byte) {
	var requestSlice []reqgql
	requestSlice = append(requestSlice, body)
	jsonReq, err := json.Marshal(requestSlice)
	if err != nil {
		fmt.Println("marshalling", err)
	}
	req, err := http.NewRequest(http.MethodPost, graphql, bytes.NewBuffer(jsonReq))
	if err != nil {
		fmt.Println("send request", err)
	}
	req.Header.Set("Client-Id", clientid)
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println("send request", err)
	}
	defer resp.Body.Close()
	response, _ = io.ReadAll(resp.Body)
	return
}
