package main

import (
	_ "embed"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io"
	"net/http"
	"strings"
	"time"
)

//go:embed .env
var env string
var graphqlparams string = strings.Split(env, " ")[0]
var csrftoken string = strings.Split(env, " ")[1]
var guestoken string = strings.Split(env, " ")[2]
var authbearer string = strings.Split(env, " ")[3]
var cookieguest string = strings.Split(env, " ")[4]
var cookiect0 string = strings.Split(env, " ")[5]
var cookiegt string = strings.Split(env, " ")[6]
var cookiepref string = strings.Split(env, " ")[7]

const graphql string = "https://twitter.com/i/api/graphql"

var follows = []string{"samueletienne", "HortyUnderscore"}

type tweet struct {
	Author   string
	FullText string
}

type resgql struct {
	Data resData `json:"data"`
}
type resData struct {
	User resUser `json:"user"`
}
type resUser struct {
	Result resResult `json:"result"`
}
type resResult struct {
	Timeline resTimeline `json:"timeline"`
}
type resTimeline struct {
	Timeline resTimelineBis `json:"timeline"`
}
type resTimelineBis struct {
	Instructions []resInstructions `json:"instructions"`
}
type resInstructions struct {
	Entries []resEntries `json:"entries"`
}
type resEntries struct {
	Content resContent `json:"content"`
}
type resContent struct {
	ItemContent resItemContent `json:"itemContent"`
}
type resItemContent struct {
	TweetResults resTweetResults `json:"tweet_results"`
}
type resTweetResults struct {
	ResultBis resResultBis `json:"result"`
}
type resResultBis struct {
	Legacy resLegacy `json:"legacy"`
}
type resLegacy struct {
	FullText string `json:"full_text"`
	Id_str   string `json:"id_str"`
}

type rss struct {
	Version     string    `xml:"version,attr"`
	Title       string    `xml:"channel>title"`
	Link        string    `xml:"channel>link"`
	Description string    `xml:"channel>description"`
	PubDate     string    `xml:"channel>pubDate"`
	Item        []rssItem `xml:"channel>item"`
}

type rssItem struct {
	Title       string `xml:"title"`
	Link        string `xml:"link"`
	Description string `xml:"description"`
}

type tweetContent struct {
	Text   string
	Author string
	Id     string
}

func main() {
	var rssTweets []rssItem
	for _, follow := range follows {
		tweetsFollow := requestTwitch(follow)
		for _, tweetFollow := range tweetsFollow {
			currentTweet := rssItem{
				Title:       tweetFollow.Text,
				Link:        "https://www.twitter.com/" + tweetFollow.Author + "/" + tweetFollow.Id,
				Description: "",
			}
			rssTweets = append(rssTweets, currentTweet)
		}
	}
	pubdateTime := time.Now().Format(time.RubyDate)
	rssFeed := rss{
		Version:     "2.0",
		Title:       "Twitter",
		Link:        "https://www.twitter.com",
		Description: "autogen rss from twitter follows",
		PubDate:     pubdateTime,
		Item:        rssTweets,
	}
	data, _ := xml.MarshalIndent(rssFeed, "", "    ")
	fmt.Println(string(data))
}

func requestTwitch(follow string) (tweets []tweetContent) {
	body := strings.NewReader("") // use of empty body only to satisfy NewRequest params
	req, err := http.NewRequest(http.MethodGet, graphql+graphqlparams, body)
	if err != nil {
		fmt.Println("send request", err)
	}
	req.Header.Set("x-csrf-token", csrftoken)
	req.Header.Set("referer", "https://twitter.com/"+follow)
	req.Header.Set("x-twitter-active-user", "yes")
	req.Header.Set("autority", "twitter.com")
	req.Header.Set("sec-ch-ua", ` "Not A;Brand";v="99", "Chromium";v="98"`)
	req.Header.Set("x-twitter-client-language", "fr")
	req.Header.Set("sec-ch-ua-mobile", "?0")
	req.Header.Set("content-type", "application/json")
	req.Header.Set("user-agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36")
	req.Header.Set("x-guest-token", guestoken)
	req.Header.Set("sec-ch-ua-platform", `"Linux"`)
	req.Header.Set("accept", "*/*")
	req.Header.Set("sec-fetch-site", "same-origin")
	req.Header.Set("sec-fetch-mode", "cors")
	req.Header.Set("sec-fetch-dest", "empty")
	req.Header.Set("accept-language", "fr-FR,fr;q=0.9")
	req.Header.Set("authorization", "Bearer "+authbearer)
	req.Header.Set("cookie", "guest_id="+cookieguest+"; ct0="+cookiect0+"; gt="+cookiegt+"; d_prefs="+cookiepref)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println("send request", err)
	}
	defer resp.Body.Close()
	response, _ := io.ReadAll(resp.Body)

	var data resgql
	err = json.Unmarshal(response, &data)
	if err != nil {
		fmt.Println("unmarshalling", err)
	}
	for _, entry := range data.Data.User.Result.Timeline.Timeline.Instructions[1].Entries {
		currentTweet := tweetContent{
			Text:   entry.Content.ItemContent.TweetResults.ResultBis.Legacy.FullText,
			Id:     entry.Content.ItemContent.TweetResults.ResultBis.Legacy.Id_str,
			Author: follow,
		}
		tweets = append(tweets, currentTweet)
	}
	return
}
