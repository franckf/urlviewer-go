package main

import (
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"net/http"
    "time"
)

type Feed struct {
	XMLName xml.Name `xml:"feed"`
	Title   string   `xml:"title"`
	Entries []Entry  `xml:"entry"`
}

type Entry struct {
	Title     string `xml:"title"`
	Link      Link   `xml:"link"`
	Published string `xml:"published"`
	Summary   string `xml:"summary"`
}

type Link struct {
	Href string `xml:"href,attr"`
}

func main() {
	// Récupérer le contenu de l'URL
	resp, err := http.Get("https://fr.finalfantasyxiv.com/lodestone/news/topics.xml")
	if err != nil {
		fmt.Println("Erreur lors de la récupération de l'URL:", err)
		return
	}
	defer resp.Body.Close()

	// Lire le corps de la réponse HTTP
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("Erreur lors de la lecture du corps de la réponse:", err)
		return
	}

	// Décoder le XML en structure Feed
	var feed Feed
	err = xml.Unmarshal(body, &feed)
	if err != nil {
		fmt.Println("Erreur lors du décodage XML:", err)
		return
	}

    pubdateTime := time.Now().Format(time.RubyDate)

	// Créer le début du flux RSS
/*
<?xml version="1.0" encoding="UTF-8"?>
<rss version="2.0">
  <channel>
    <title></title>
    <link></link>
    <description></description>
    <item>
      <title></title>
      <link></link>
      <description></description>
    </item>
  </channel>
</rss>
*/
	rss := "<?xml version=\"1.0\" encoding=\"UTF-8\"?><rss version=\"2.0\"><channel><title>" + feed.Title + "</title><link>https://fr.finalfantasyxiv.com/lodestone</link><description>test</description><pubDate>" + pubdateTime + "</pubDate>"

	// Ajouter chaque entrée comme un item du flux RSS
	for _, entry := range feed.Entries {
        rss += "<item><title>" + entry.Title + "</title><link>" + entry.Link.Href + "</link><description>" + "test" + "</description></item> \n"
	}

	// Fermer le flux RSS
	rss += "</channel></rss>"

	// Afficher le résultat
	fmt.Println(rss)
}

